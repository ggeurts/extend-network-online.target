# extend network-online.target

Example service to extend the systemd target network-online to let the target only be reached when a DNS name is able to be resolved.
----------------------------------------
The question 'When is a network "up"? can be answered in X ways. In reality it changes per machine. You don't want to have service XYZ started before situation ABC. 
Network "up" is one of these situations and thus for example network filesystems like nfs mounts are only performed after systemd reached the target network-online.target.

When however the networked filesystem is described by host.domain:/export, then ip reachablility is not enough. The name host.domain should first be resolvable. 

On CentOS 7 the name resolution apparently functions slightly later then when the network is flaged as "up" and nfs mounts fail with "mount.nfs: Failed to resolve server host.domain: Name or service not known".

I wrote a service to wait until a successful query of host.domain and made the network-online.target dependant of this service. This service can be used as a template for tests when the network can be flaged as "up"


**Installation**


**1. copy and rename**

copy the service to /etc/systemd/system/ and optionally rename it.

`wget -O /etc/systemd/system/wait_for_functioning_dns.service https://gitlab.com/ggeurts/extend-network-online.target/-/raw/master/etc/systemd/system/wait_for_functioning_dns.service` 

*optionally:*<br>
`mv /etc/systemd/system/{wait_for_dns_to_function.service,wait_for_something_else_to_consider_network_up.service}`

**2. modify the ExecStart scriplet**

Change the scriplet or change the ExecStart to the full path to a script with a waits untill a test is successful to determin if the network is to be considered "up".

*For the template script to function as intended the "**host.domain**" should be modified to a name that you want to be able to resolve before you consider the network to be up.*

**3. enable the service**

`systemctl enable wait_for_functioning_dns.service`